# Demo

This project allows you to bootstrap new server and deploy java web service 

### Tools
This project is using the following list of tools:

1. Jenkins
2. Ansible
3. AWS EC2 instances
4. Bitbucket

### Deploying new serwer

1. Login to Jenkins
http://ec2-34-217-211-248.us-west-2.compute.amazonaws.com:8080
2. Run Demo job (with parameters)
3. Enter environment name (e.g. dev)
4. Enter e-mail for notification after failed deployment
5. Click Build

Below is sample output from the job execution

```
/*Started by user demo
Obtained Jenkinsfile from git git@bitbucket.org:dawidszymaniuk/demo.git
Running in Durability level: MAX_SURVIVABILITY
[Pipeline] node
Running on Jenkins in /var/lib/jenkins/workspace/Demo
[Pipeline] {
[Pipeline] stage
[Pipeline] { (Checkout)
[Pipeline] checkout
 > git rev-parse --is-inside-work-tree # timeout=10
Fetching changes from the remote Git repository
 > git config remote.origin.url git@bitbucket.org:dawidszymaniuk/demo.git # timeout=10
Fetching upstream changes from git@bitbucket.org:dawidszymaniuk/demo.git
 > git --version # timeout=10
using GIT_SSH to set credentials 
 > git fetch --tags --progress git@bitbucket.org:dawidszymaniuk/demo.git +refs/heads/*:refs/remotes/origin/*
 > git rev-parse refs/remotes/origin/master^{commit} # timeout=10
 > git rev-parse refs/remotes/origin/origin/master^{commit} # timeout=10
Checking out Revision 9c4e3d996fab3e2a3ac26aeedeed8917fc909e25 (refs/remotes/origin/master)
 > git config core.sparsecheckout # timeout=10
 > git checkout -f 9c4e3d996fab3e2a3ac26aeedeed8917fc909e25
Commit message: "a"
 > git rev-list --no-walk 9c4e3d996fab3e2a3ac26aeedeed8917fc909e25 # timeout=10
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (Bootstrap server)
[Pipeline] withCredentials
[Pipeline] {
[Pipeline] withEnv
[Pipeline] {
[Pipeline] wrap
[Pipeline] {
[Pipeline] ansiblePlaybook
[Demo] $ ansible-playbook demo.yml -e env=prod
 [WARNING]: provided hosts list is empty, only localhost is available. Note
that the implicit localhost does not match 'all'


PLAY [Create a sandbox instance] ***********************************************

TASK [Launch instance] *********************************************************
changed: [localhost]

TASK [Add new instance to host group] ******************************************
changed: [localhost] => (item={u'kernel': None, u'root_device_type': u'ebs', u'private_dns_name': u'ip-172-31-20-53.us-west-2.compute.internal', u'public_ip': u'54.202.151.253', u'private_ip': u'172.31.20.53', u'id': u'i-0058129c04778b6fa', u'ebs_optimized': False, u'state': u'running', u'virtualization_type': u'hvm', u'root_device_name': u'/dev/sda1', u'ramdisk': None, u'block_device_mapping': {u'/dev/sda1': {u'status': u'attached', u'delete_on_termination': True, u'volume_id': u'vol-09a8564269606d039'}}, u'key_name': u'centos', u'image_id': u'ami-2adb4752', u'tenancy': u'default', u'groups': {u'sg-2d146452': u'launch-wizard-2'}, u'public_dns_name': u'ec2-54-202-151-253.us-west-2.compute.amazonaws.com', u'state_code': 16, u'tags': {u'environment': u'prod'}, u'placement': u'us-west-2a', u'ami_launch_index': u'0', u'dns_name': u'ec2-54-202-151-253.us-west-2.compute.amazonaws.com', u'region': u'us-west-2', u'launch_time': u'2018-04-17T08:03:28.000Z', u'instance_type': u't2.micro', u'architecture': u'x86_64', u'hypervisor': u'xen'})

TASK [Wait for SSH to come up] *************************************************
ok: [localhost] => (item={u'kernel': None, u'root_device_type': u'ebs', u'private_dns_name': u'ip-172-31-20-53.us-west-2.compute.internal', u'public_ip': u'54.202.151.253', u'private_ip': u'172.31.20.53', u'id': u'i-0058129c04778b6fa', u'ebs_optimized': False, u'state': u'running', u'virtualization_type': u'hvm', u'root_device_name': u'/dev/sda1', u'ramdisk': None, u'block_device_mapping': {u'/dev/sda1': {u'status': u'attached', u'delete_on_termination': True, u'volume_id': u'vol-09a8564269606d039'}}, u'key_name': u'centos', u'image_id': u'ami-2adb4752', u'tenancy': u'default', u'groups': {u'sg-2d146452': u'launch-wizard-2'}, u'public_dns_name': u'ec2-54-202-151-253.us-west-2.compute.amazonaws.com', u'state_code': 16, u'tags': {u'environment': u'prod'}, u'placement': u'us-west-2a', u'ami_launch_index': u'0', u'dns_name': u'ec2-54-202-151-253.us-west-2.compute.amazonaws.com', u'region': u'us-west-2', u'launch_time': u'2018-04-17T08:03:28.000Z', u'instance_type': u't2.micro', u'architecture': u'x86_64', u'hypervisor': u'xen'})

TASK [Refresh inventory cache] *************************************************
changed: [localhost]

PLAY RECAP *********************************************************************
localhost                  : ok=4    changed=3    unreachable=0    failed=0   

[Pipeline] }
[Pipeline] // wrap
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] // withCredentials
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (Deploy java server)
[Pipeline] withCredentials
[Pipeline] {
[Pipeline] withEnv
[Pipeline] {
[Pipeline] wrap
[Pipeline] {
[Pipeline] ansiblePlaybook
[Demo] $ ansible-playbook run.yml -i ec2.py --private-key /var/lib/jenkins/workspace/Demo/ssh7968035901358770745.key -u centos -e env=prod

PLAY [Configure instance] ******************************************************

TASK [Gathering Facts] *********************************************************
ok: [54.202.151.253]

TASK [Install java] ************************************************************
changed: [54.202.151.253]

TASK [Setup logrotate] *********************************************************
changed: [54.202.151.253]

TASK [Download jar] ************************************************************
changed: [54.202.151.253]

TASK [Check if server is running] **********************************************
fatal: [54.202.151.253]: FAILED! => {"changed": true, "cmd": "ps -ef | grep server.jar | grep -v grep", "delta": "0:00:00.042291", "end": "2018-04-17 08:10:11.005752", "msg": "non-zero return code", "rc": 1, "start": "2018-04-17 08:10:10.963461", "stderr": "", "stderr_lines": [], "stdout": "", "stdout_lines": []}
...ignoring

TASK [Start java service] ******************************************************
changed: [54.202.151.253]

TASK [Wait for port 8081] ******************************************************
ok: [54.202.151.253]

TASK [Check server status] *****************************************************
ok: [54.202.151.253]

TASK [fail] ********************************************************************
skipping: [54.202.151.253]

TASK [debug] *******************************************************************
ok: [54.202.151.253] => {
    "msg": "Success - status ok"
}

PLAY RECAP *********************************************************************
54.202.151.253             : ok=9    changed=5    unreachable=0    failed=0   

[Pipeline] }
[Pipeline] // wrap
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] // withCredentials
[Pipeline] }
[Pipeline] // stage
[Pipeline] }
[Pipeline] // node
[Pipeline] End of Pipeline
Finished: SUCCESS
```


